//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================
using GameFrame;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Avalonia_UI
{
    public partial class MainWindow : Window
    {


        string Text { get; set; }

        public MainWindow()
        {

            InitializeComponent();                   
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        void StartGame(object sender, RoutedEventArgs e)
        {
            LoadCartridgeAsync(sender,e);
            
        }

        async Task LoadCartridgeAsync(object sender, EventArgs e)
        {
            
            string path = await GetPath();
            StartNesGame.Start(path);
        }

        public async Task<string> GetPath()
        {
            var dialog = new OpenFileDialog();
            var result = await dialog.ShowAsync(this);

            if (result == null)
            {
                await GetPath();
            }

            return result.FirstOrDefault();
        }
       

    }
}
