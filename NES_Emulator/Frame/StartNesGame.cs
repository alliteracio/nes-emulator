//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================
using System;
using System.Windows.Forms;

namespace GameFrame
{
    public static class StartNesGame
    {
        static void Main()
        { 
        
        }
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Start(string path)
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new GameView(path));
        }
    }
}
