﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================

using Apro.Alexandra.Nes.Internals;
using Apro.Alexandra.Nes.Memories;
using System.Reflection;

namespace Apro.Alexandra.Nes
{
    public class Cpu6502
    {
        public int Cycles { get; set; }
        public byte A { get; set; }
        public byte X { get; set; }
        public byte Y { get; set; }
        public byte SP { get; set; }
        public ushort PC { get; set; }

        public bool C; // Carry flag
        public bool Z; // Zero flag
        public bool I; // Interrpt Disable
        public bool D; // Decimal Mode (Not used)
        public bool B; // Break command
        public bool V; // Overflow flag
        public bool N; // Negative flag

        public enum AddressMode
        {
            Absolute = 1,    // 1
            AbsoluteX,       // 2
            AbsoluteY,       // 3
            Accumulator,     // 4
            Immediate,       // 5
            Implied,         // 6
            IndexedIndirect, // 7
            Indirect,        // 8
            IndirectIndexed, // 9
            Relative,        // 10
            ZeroPage,        // 11
            ZeroPageX,       // 12
            ZeroPageY        // 13
        };

        public readonly CpuMemory memory;
        public InstructionDecoder lookup;   
        
        private bool irqInterrupt;
        private bool nmiInterrupt;
        private ushort address = 0;
        private bool pageCrossed = false;
        private int counter;

        public Cpu6502(Bus bus)
        {
            memory = bus.CpuRam;
            lookup = new InstructionDecoder(this);
        }

        public int Clock()
        {              
            if(counter == 0)
            {
                if (irqInterrupt) Irq();
                irqInterrupt = false;

                if (nmiInterrupt) Nmi();
                nmiInterrupt = false;

                int cycle = Cycles;
                byte opCode = memory.Read(PC);

                Instruction instruction = lookup[opCode];
                AddressMode mode = instruction.Addrmode.Invoke();

                PC += (ushort)instruction.Size;
                Cycles += instruction.Cycles;

                if (pageCrossed) Cycles += instruction.PageCycles;
                instruction.Operate.Invoke(mode, address);

                return Cycles - cycle;
            }

            counter--;
            return 1;
        }
        public void Reset()
        {
            PC = memory.Read16(0xFFFC);
            SP = 0xFD;
            A = 0;
            X = 0;
            Y = 0;
            SetProcessorFlags((byte)0x24);

            Cycles = 0;
            counter = 0;

            nmiInterrupt = false;
        }
        public void TriggerNmi()
        {
            nmiInterrupt = true;
        }
        public void Irq()
        {
            PushStack16(PC);
            PushStack(GetStatusFlags());
            PC = memory.Read16(0xFFFE);
            I = true;
        }
        public void Nmi()
        {
            PushStack16(PC);
            PushStack(GetStatusFlags());
            PC = memory.Read16(0xFFFA);
            I = true;
        }     
        public void AddIdleCycles(int idleCycles)
        {
            counter += idleCycles;
        }

        //--------------------------------------------
        public AddressMode ACC()
        {
            return AddressMode.Accumulator;
        }
        public AddressMode IMP()
        {
            return AddressMode.Implied;
        }
        public AddressMode IMM()
        {
            address = (ushort)(PC + 1);
            return AddressMode.Immediate;
        }
        public AddressMode ZP0()
        {
            address = memory.Read((ushort)(PC + 1));
            return AddressMode.ZeroPage;
        }
        public AddressMode ZPX()
        {
            address = (ushort)((memory.Read((ushort)(PC + 1)) + X) & 0xFF);
            return AddressMode.ZeroPageX;
        }
        public AddressMode ZPY()
        {
            address = (ushort)((memory.Read((ushort)(PC + 1)) + Y) & 0xFF);
            return AddressMode.ZeroPageY;
        }
        public AddressMode REL()
        {
            address = (ushort)(PC + (sbyte)memory.Read((ushort)(PC + 1)) + 2);
            return AddressMode.Relative;
        }
        public AddressMode ABS()
        {
            address = memory.Read16((ushort)(PC + 1));
            return AddressMode.Absolute;
        }
        public AddressMode ABX()
        {
            address = (ushort)(memory.Read16((ushort)(PC + 1)) + X);
            pageCrossed = IsPageCross((ushort)(address - X), (ushort)X);
            return AddressMode.AbsoluteX;
        }
        public AddressMode ABY()
        {
            address = (ushort)(memory.Read16((ushort)(PC + 1)) + Y);
            pageCrossed = IsPageCross((ushort)(address - Y), (ushort)Y);
            return AddressMode.AbsoluteY;
        }
        public AddressMode IND()
        {
            address = (ushort)memory.Read16WrapPage((ushort)memory.Read16((ushort)(PC + 1)));
            return AddressMode.Indirect;
        }
        public AddressMode IZX()
        {
            ushort lowerNibbleAddress = (ushort)((memory.Read((ushort)(PC + 1)) + X) & 0xFF);
            address = (ushort)memory.Read16WrapPage((ushort)(lowerNibbleAddress));
            return AddressMode.IndexedIndirect;
        }
        public AddressMode IZY()
        {
            ushort valueAddress = (ushort)memory.Read((ushort)(PC + 1));
            address = (ushort)(memory.Read16WrapPage(valueAddress) + Y);
            pageCrossed = IsPageCross((ushort)(address - Y), address);
            return AddressMode.IndirectIndexed;
        }
        //--------------------------------------------
        public byte GetStatusFlags()
        {
            byte flags = 0;

            if (C) flags |= (byte)(1 << 0);
            if (Z) flags |= (byte)(1 << 1);
            if (I) flags |= (byte)(1 << 2);
            if (D) flags |= (byte)(1 << 3);
            if (B) flags |= (byte)(1 << 4);
            flags |= (byte)(1 << 5);
            if (V) flags |= (byte)(1 << 6);
            if (N) flags |= (byte)(1 << 7);

            return flags;
        }
        public void SetProcessorFlags(byte flags)
        {
            C = IsBitSet(flags, 0);
            Z = IsBitSet(flags, 1);
            I = IsBitSet(flags, 2);
            D = IsBitSet(flags, 3);
            B = IsBitSet(flags, 4);
            V = IsBitSet(flags, 6);
            N = IsBitSet(flags, 7);
        }
        public void SetSpecialFlags(byte value)
        {
            Z = value == 0;
            N = ((value >> 7) & 1) == 1;
        }
        public bool IsPageCross(ushort i, ushort j)
        {
            return (i & 0xFF) != (j & 0xFF);
        }
        public bool IsBitSet(byte value, int index)
        {
            return (value & (1 << index)) != 0;
        }
        public byte PullStack()
        {
            SP++;
            byte data = memory.Read((ushort)(0x0100 | SP));
            return data;
        }
        public ushort PullStack16()
        {
            byte lo = PullStack();
            byte hi = PullStack();
            return (ushort)((hi << 8) | lo);
        }
        public void PushStack(byte data)
        {
            memory.Write((ushort)(0x100 | SP), data);
            SP--;
        }
        public void PushStack16(ushort data)
        {
            byte lo = (byte)(data & 0xFF);
            byte hi = (byte)((data >> 8) & 0xFF);

            PushStack(hi);
            PushStack(lo);
        }
        public void HandleBranchCycles(ushort origPc, ushort branchPc)
        {
            Cycles++;
            Cycles += IsPageCross(origPc, branchPc) ? 1 : 0;
        }

    }
}