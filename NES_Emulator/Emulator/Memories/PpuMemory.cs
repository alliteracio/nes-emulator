﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================
using System;

namespace Apro.Alexandra.Nes.Memories
{
    public class PpuMemory : Memory
    {
        readonly Bus bus;
        readonly byte[] currentRam;
        readonly byte[] paletteRam;

        public PpuMemory(Bus console)
        {
            bus = console;
            currentRam = new byte[2048];
            paletteRam = new byte[32];
        }

        public void Reset()
        {
            Array.Clear(currentRam, 0, currentRam.Length);
            Array.Clear(paletteRam, 0, paletteRam.Length);
        }

        public ushort GetPaletteRamIndex(ushort address)
        {
            ushort index = (ushort)((address - 0x3F00) % 32);

            if (index >= 16 && ((index - 16) % 4 == 0)) return (ushort)(index - 16);
            else return index;
        }

        public override byte Read(ushort address)
        {
            byte data;
            if (address < 0x2000) 
            {
                data = bus.Mapper.Read(address);
            }
            else if (address <= 0x3EFF) 
            {
                data = currentRam[bus.Mapper.CurrentRamAddressToIndex(address)];
            }
            else if (address >= 0x3F00 && address <= 0x3FFF) 
            {
                data = paletteRam[GetPaletteRamIndex(address)];
            }
            else 
            {
                throw new Exception("Hibás Ppu olvasás: " + address.ToString("x4"));
            }
            return data;
        }

        public override void Write(ushort address, byte data)
        {
            if (address < 0x2000)
            {
                bus.Mapper.Write(address, data);
            }
            else if (address >= 0x2000 && address <= 0x3EFF) 
            {
                currentRam[bus.Mapper.CurrentRamAddressToIndex(address)] = data;
            }
            else if (address >= 0x3F00 && address <= 0x3FFF) 
            {
                ushort addr = GetPaletteRamIndex(address);
                paletteRam[addr] = data;
            }
            else 
            {
                throw new Exception("Hibás Ppu írás: " + address.ToString("x4"));
            }
        }
    }
}
