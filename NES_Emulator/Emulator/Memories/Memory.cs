﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================

namespace Apro.Alexandra.Nes.Memories
{
    public abstract class Memory
    {
      
        public abstract byte Read(ushort address);

        public abstract void Write(ushort address, byte data);

        public void ReadBufWrapping(byte[] buffer, int startIndex, ushort startAddress, int size)
        {
            int index = startIndex;
            int bytesRead = 0;
            ushort address = startAddress;
            while (bytesRead < size)
            {
                if (index >= buffer.Length) index = 0;
                buffer[index] = Read(address);

                address++;
                bytesRead++;
                index++;
            }
        }

        public ushort Read16(ushort address)
        {
            byte lo = Read(address);
            byte hi = Read((ushort)(address + 1));
            return (ushort)((hi << 8) | lo);
        }

        public ushort Read16WrapPage(ushort address)
        {
            ushort data;
            if ((address & 0xFF) == 0xFF)
            {
                byte lo = Read(address);
                byte hi = Read((ushort)(address & (~0xFF))); // Wrap around to start of page eg. 0x02FF becomes 0x0200
                data = (ushort)((hi << 8) | lo);
            }
            else
            {
                data = Read16(address);
            }
            return data;
        }
    }
}
