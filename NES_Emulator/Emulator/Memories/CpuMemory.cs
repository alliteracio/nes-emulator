﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================
using System;

namespace Apro.Alexandra.Nes.Memories
{
    public class CpuMemory : Memory
    { 
    private readonly byte[] internalRam = new byte[2048];
    private readonly Bus bus;

    public CpuMemory(Bus Bus)
    {
        bus = Bus;
    }

    public void Reset()
    {
        Array.Clear(internalRam, 0, internalRam.Length);
    }

    ushort HandleInternalRamMirror(ushort address)
    {
        return (ushort)(address % 0x800);
    }

    ushort GetPpuRegisterFromAddress(ushort address)
    {    
        if (address == 0x4014) return address;
        else return (ushort)(0x2000 + ((address - 0x2000) % 8));
    }

    void WritePpuRegister(ushort address, byte data)
    {
        bus.Ppu.Write(GetPpuRegisterFromAddress(address), data);
    }

    byte ReadPpuRegister(ushort address)
    {
        return bus.Ppu.Read(GetPpuRegisterFromAddress(address));
    }

    byte ReadApuIoRegister(ushort address)
    {
        byte data;
        switch (address)
        {
            case 0x4016: // Controller
                data = bus.Controller.GetControllerOutput();
                break;
            default: 
                data = 0;
                break;
        }
        return data;
    }

    void WriteApuIoRegister(ushort address, byte data)
    {
        switch (address)
        {
            case 0x4016: // Controller 1
                bus.Controller.SetControllerInput(data);
                break;
            default: // Unimplemented register
                data = 0;
                break;
        }
    }

    public override byte Read(ushort address)
    {
        byte data;
        if (address < 0x2000) 
        {
            ushort addressIndex = HandleInternalRamMirror(address);
            data = internalRam[addressIndex];
        }
        else if (address <= 0x3FFF) 
        {
            data = ReadPpuRegister(address);
        }
        else if (address <= 0x4017) 
        {
            data = ReadApuIoRegister(address);
        }
        else if (address <= 0x401F) 
        {
            data = 0;
        }
        else if (address >= 0x4020) 
        {
            data = bus.Mapper.Read(address);
        }
        else 
        {
            throw new Exception("Hibás memória olvasás: " + address.ToString("X4"));
        }

        return data;
    }


    public override void Write(ushort address, byte data)
    {
        if (address < 0x2000) 
        {
            ushort addressIndex = HandleInternalRamMirror(address);
            internalRam[addressIndex] = data;
        }
        else if (address <= 0x3FFF || address == 0x4014) 
        {
            WritePpuRegister(address, data);
        }
        else if (address <= 0x4017) 
        {
            WriteApuIoRegister(address, data);
        }
        else if (address <= 0x401F) 
        {

        }
        else if (address >= 0x4020)
        {
            bus.Mapper.Write(address, data);
        }
        else 
        {
            throw new Exception("Hibás memória irás: " + address.ToString("X4"));
        }
    }
}
}
