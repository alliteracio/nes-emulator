﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================
using Apro.Alexandra.Nes.Memories;
using System;

namespace Apro.Alexandra.Nes
{
    public class Ppu2c02
	{
        private readonly PpuMemory memory;
        private readonly Bus bus;

        private byte[] oam;
        private ushort oamAddress;
        private byte[] sprites;
        private int[] spriteIndicies;
        private int spritesCount;

        public int Scanline { get; private set; }     
        public int Cycle { get; private set; }
        public byte[] BitmapData { get; }

        private ushort backGroundAddress;
        private ushort spriteAdress;
        private int currentIncrement;
        private byte last;

        private byte overflowFlag;
        private byte zeroFlag;
        private byte incrementFlag;
        private byte spriteAddressFlag;
        private byte backGroundAddressFlag;
        private byte sizeFlag;
        private byte interruptionFlag;
        private byte occurredFlag;
        private byte leftBackGroundFlag;
        private byte leftSpriteFlag;
        private byte backGroundFlag;
        private byte spriteFlag;

        private ushort current;
        private ushort temporary;
        private byte xReg;
        private byte writeNumber;
        private byte fReg;

        private ulong tileShift;
        private byte nameTable;
        private byte attributeTable;
        private byte lo;
        private byte hi;
        private byte buffer;
     
        public Ppu2c02(Bus Bus)
        {
            memory = Bus.PpuRam;
            bus = Bus;

            BitmapData = new byte[256 * 240];

            oam = new byte[256];
            sprites = new byte[32];
            spriteIndicies = new int[8];
        }

        public void Reset()
        {
            Array.Clear(BitmapData, 0, BitmapData.Length);

            Scanline = 240;
            Cycle = 340;

            occurredFlag = 0;
            interruptionFlag = 0;

            writeNumber = 0;
            fReg = 0;

            Array.Clear(oam, 0, oam.Length);
            Array.Clear(sprites, 0, sprites.Length);
        }

        byte CreateBackGroundPalette(byte data)
        {
            int color = data & 0x3;
            if (color == 0)
            {
                return memory.Read(0x3F00);
            }
       
            switch ((data >> 2) & 0x3)
            {
                case 0:
                    return memory.Read((ushort)(0x3F01 + (color - 1)));
                case 1:
                    return memory.Read((ushort)(0x3F05 + (color - 1)));
                case 2:
                    return memory.Read((ushort)(0x3F09 + (color - 1)));
                case 3:
                    return memory.Read((ushort)(0x3F0D + (color - 1)));
                default:
                    throw new Exception("Ismererlen paletta.");
            }
        }

        byte CreateSpritePalette(byte data)
        {
            int color = data & 0x3;
            if (color == 0)
            {
                return memory.Read(0x3F00);
            }

            switch ((data >> 2) & 0x3)
            {
                case 0:
                    return memory.Read((ushort)(0x3F11 + (color - 1)));
                case 1:
                    return memory.Read((ushort)(0x3F15 + (color - 1)));
                case 2:
                    return memory.Read((ushort)(0x3F19 + (color - 1)));
                case 3:
                    return memory.Read((ushort)(0x3F1D + (color - 1)));
                default:
                    throw new Exception("Ismeretlen paletta");
            }
        }

        byte CreateSprite(out int index)
        {
            int x = Cycle - 1;
            int y = Scanline - 1;

            index = 0;

            if (spriteFlag == 0)
            {
                return 0;
            }

            if (leftSpriteFlag == 0 && x < 8)
            {
                return 0;
            }

            ushort currentAdress = spriteAdress;

            for (int i = 0; i < spritesCount * 4; i += 4)
            {
                int left = sprites[i + 3];
                int offset = x - left;

                if (offset <= 7 && offset >= 0)
                {
                    int yOffset = y - sprites[i];

                    byte pattern;

                    if (sizeFlag == 1)
                    {
                        currentAdress = (ushort)((sprites[i + 1] & 1) * 0x1000);
                        pattern = (byte)(sprites[i + 1] & 0xFE);
                    }
                    else
                    {
                        pattern = (byte)(sprites[i + 1]);
                    }

                    ushort patternAddress = (ushort)(currentAdress + (pattern * 16));

                    bool horisontalFlipped = (sprites[i + 2] & 0x40) != 0;
                    bool verticalFlipped = (sprites[i + 2] & 0x80) != 0;
                    int color = GetPattern(patternAddress, offset, yOffset, horisontalFlipped, verticalFlipped);

                    if (color == 0)
                    {
                        continue;
                    }
                    else 
                    {
                        byte palette = (byte)(sprites[i + 2] & 0x03);
                        index = i / 4;
                        return (byte)(((palette << 2) | color) & 0xF);
                    }
                }
            }
            return 0x00; 
        }

        int GetPattern(ushort adress, int x, int y, bool horisontalFlip = false, bool verticalFlip = false)
        {
            int h = sizeFlag == 0 ? 7 : 15;
            x = horisontalFlip ? 7 - x : x;
            y = verticalFlip ? h - y : y;

           
            ushort yAddress;
            if (y <= 7) yAddress = (ushort)(adress + y);
            else yAddress = (ushort)(adress + 16 + (y - 8)); 

           
            byte[] pattern = new byte[2];
            pattern[0] = memory.Read(yAddress);
            pattern[1] = memory.Read((ushort)(yAddress + 8));
            return (((byte)((pattern[1] >> (7 - x)) & 1) << 1) | (byte)((pattern[0] >> (7 - x)) & 1)) & 0x03;
        }
       
        public void Clock()
        {
            CareInterruption();

            if (RenderingEnabled())
            {
                if (Scanline == 261 && fReg == 1 && Cycle == 339)
                {
                    fReg ^= 1;
                    Scanline = 0;
                    Cycle = -1;
                    bus.DrawFrame();
                    return;
                }
            }
            Cycle++;


            if (Cycle > 340)
            {
                if (Scanline == 261)
                {
                    fReg ^= 1;
                    Scanline = 0;
                    Cycle = -1;
                    bus.DrawFrame();
                }
                else
                {
                    Cycle = -1;
                    Scanline++;
                }
            }

            if ((Scanline == 261) && Cycle == 1)
            {
                SetFlags();
            }

            if (RenderingEnabled())
            {

                if (Cycle == 257)
                {
                    if ((Scanline >= 0 && Scanline < 240))
                    {
                        Array.Clear(sprites, 0, sprites.Length);
                        Array.Clear(spriteIndicies, 0, spriteIndicies.Length);

                        int h = sizeFlag == 0 ? 7 : 15;
                        spritesCount = 0;
                        int y = Scanline;

                        for (int i = oamAddress; i < 256; i += 4)
                        {
                            byte top = oam[i];
                            int offset = y - top;

                            if (offset <= h && offset >= 0)
                            {
                                if (spritesCount == 8)
                                {
                                    overflowFlag = 1;
                                    break;
                                }
                                else
                                {
                                    Array.Copy(oam, i, sprites, spritesCount * 4, 4);
                                    spriteIndicies[spritesCount] = (i - oamAddress) / 4;
                                    spritesCount++;

                                }
                            }
                        }
                    }
                    else
                    {
                        spritesCount = 0;
                    }
                }

                if ((Cycle > 0 && Cycle <= 256) && (Scanline >= 0 && Scanline < 240))
                {
                    byte backGroundData = (backGroundFlag == 0 || (leftBackGroundFlag == 0 && (Cycle - 1) < 8)) ? (byte)0 : (byte)((tileShift >> (xReg * 4)) & 0xF);
                    int index;
                    byte spriteData = CreateSprite(out index);
                    bool isZero = spriteIndicies[index] == 0;
                    int backGroundColor = backGroundData & 0x03;
                    int spriteColor = spriteData & 0x03;

                    byte color;

                    if (backGroundColor == 0)
                    {
                        if (spriteColor == 0)
                        {
                            color = CreateBackGroundPalette(backGroundData);
                        }
                        else
                        {
                            color = CreateSpritePalette(spriteData);
                        }
                    }
                    else
                    {
                        if (spriteColor == 0) color = CreateBackGroundPalette(backGroundData);
                        else
                        {
                            if (isZero)
                            {
                                zeroFlag = 1;
                            }

                            int priority = (sprites[(index * 4) + 2] >> 5) & 1;
                            if (priority == 1)
                            {
                                color = CreateBackGroundPalette(backGroundData);
                            }
                            else
                            {
                                color = CreateSpritePalette(spriteData);
                            }
                        }
                    }

                    BitmapData[Scanline * 256 + (Cycle - 1)] = color;
                }

                if (((Cycle > 0 && Cycle <= 256) || (Cycle >= 321 && Cycle <= 336)) && ((Scanline >= 0 && Scanline < 240) || (Scanline == 261)))
                {
                    tileShift >>= 4;
                    switch (Cycle % 8)
                    {
                        case 1:
                            nameTable = memory.Read((ushort)(0x2000 | (current & 0x0FFF)));
                            break;
                        case 3:
                            attributeTable = memory.Read((ushort)(0x23C0 | (current & 0x0C00) | ((current >> 4) & 0x38) | ((current >> 2) & 0x07)));
                            break;
                        case 5:
                            lo = memory.Read((ushort)(backGroundAddress + (nameTable * 16) + ((current >> 12) & 0x7)));
                            break;
                        case 7:
                            hi = memory.Read((ushort)(backGroundAddress + (nameTable * 16) + ((current >> 12) & 0x7) + 8));
                            break;
                        case 0:
                            byte _palette = (byte)((attributeTable >> (((current & 0x1f) & 0x2) | ((((current >> 5) & 0x1f) & 0x2) << 1))) & 0x3);
                            ulong data = 0;

                            for (int i = 0; i < 8; i++)
                            {
                                byte loColor = (byte)((lo >> (7 - i)) & 1);
                                byte hiColor = (byte)((hi >> (7 - i)) & 1);
                                byte color = (byte)((hiColor << 1) | (loColor) & 0x03);
                                byte pixel = (byte)(((_palette << 2) | color) & 0xF);

                                data |= (uint)(pixel << (4 * i));
                            }

                            tileShift &= 0xFFFFFFFF;
                            tileShift |= (data << 32);

                            if ((current & 0x001F) == 31)
                            {
                                current = (ushort)(current & (~0x001F));
                                current = (ushort)(current ^ 0x0400);
                            }
                            else
                            {
                                current++;
                            }

                            if (Cycle == 256)
                            {
                                if ((current & 0x7000) != 0x7000)
                                {
                                    current += 0x1000;
                                }
                                else
                                {
                                    current = (ushort)(current & ~0x7000);
                                    int y = (current & 0x03E0) >> 5;
                                    if (y == 29)
                                    {
                                        y = 0;
                                        current = (ushort)(current ^ 0x0800);
                                    }
                                    else if (y == 31)
                                    {
                                        y = 0;
                                    }
                                    else
                                    {
                                        y += 1;
                                    }
                                    current = (ushort)((current & ~0x03E0) | (y << 5));
                                }
                            }

                            break;
                    }

                }


                if (Cycle > 257 && Cycle <= 320 && ((Scanline == 261) || (Scanline >= 0 && Scanline < 240)))
                {
                    oamAddress = 0;
                }

                if (Cycle == 257 && ((Scanline >= 0 && Scanline < 240) || (Scanline == 261)))
                {
                    current = (ushort)((current & 0x7BE0) | (temporary & 0x041F));
                }

                if (Cycle >= 280 && Cycle <= 304 && Scanline == 261)
                {
                    current = (ushort)((current & 0x041F) | (temporary & 0x7BE0));
                }
            }
        }

        private bool RenderingEnabled()
        {
            return (backGroundFlag != 0) || (spriteFlag != 0);
        }

        private void SetFlags()
        {
            occurredFlag = 0;
            overflowFlag = 0;
            zeroFlag = 0;
        }

        private void CareInterruption()
        {
            if (Scanline == 241 && Cycle == 1)
            {
                occurredFlag = 1;
                if (interruptionFlag != 0) bus.Cpu.TriggerNmi();
            }
        }

        public byte Read(ushort address)
        {
            byte data;
            switch (address)
            {
                case 0x2002:
                    byte retVal = 0;
                    retVal |= (byte)(last & 0x1F);
                    retVal |= (byte)(overflowFlag << 5);
                    retVal |= (byte)(zeroFlag << 6);
                    retVal |= (byte)(occurredFlag << 7);
                    occurredFlag = 0;
                    writeNumber = 0;
                    data = retVal;
                    break;
                case 0x2004:
                    data = oam[oamAddress];
                    break;
                case 0x2007:
                    byte temp = memory.Read(current);
                    if (current < 0x3F00)
                    {
                        byte bufferedData = buffer;
                        buffer = temp;
                        temp = bufferedData;
                    }
                    else
                    {
                        buffer = memory.Read((ushort)(current - 0x1000));
                    }
                    current += (ushort)(currentIncrement);
                    data = temp;
                    break;
                default:
                    throw new Exception("Hiba olvasás közben.");
            }

            return data;
        }

        public void Write(ushort address, byte data)
        {
            last = data;
            switch (address)
            {
                case 0x2000:
                    incrementFlag = (byte)((data >> 2) & 1);
                    spriteAddressFlag = (byte)((data >> 3) & 1);
                    backGroundAddressFlag = (byte)((data >> 4) & 1);
                    sizeFlag = (byte)((data >> 5) & 1);
                    interruptionFlag = (byte)((data >> 7) & 1);
                    currentIncrement = (incrementFlag == 0) ? 1 : 32;
                    backGroundAddress = (ushort)(backGroundAddressFlag == 0 ? 0x0000 : 0x1000);
                    spriteAdress = (ushort)(0x1000 * spriteAddressFlag);
                    temporary = (ushort)((temporary & 0xF3FF) | ((data & 0x03) << 10));
                    break;
                case 0x2001:
                    leftBackGroundFlag = (byte)((data >> 1) & 1);
                    leftSpriteFlag = (byte)((data >> 2) & 1);
                    backGroundFlag = (byte)((data >> 3) & 1);
                    spriteFlag = (byte)((data >> 4) & 1);
                    break;
                case 0x2003:
                    oamAddress = data;
                    break;
                case 0x2004:
                    oam[oamAddress] = data;
                    oamAddress++;
                    break;
                case 0x2005:
                    if (writeNumber == 0)
                    {
                        temporary = (ushort)((temporary & 0xFFE0) | (data >> 3));
                        xReg = (byte)(data & 0x07);
                        writeNumber = 1;
                    }
                    else
                    {
                        temporary = (ushort)(temporary & 0xC1F);
                        temporary |= (ushort)((data & 0x07) << 12);
                        temporary |= (ushort)((data & 0xF8) << 2);
                        writeNumber = 0;
                    }
                    break;
                case 0x2006:
                    if (writeNumber == 0)
                    {
                        temporary = (ushort)((temporary & 0x00FF) | (data << 8));
                        writeNumber = 1;
                    }
                    else
                    {
                        temporary = (ushort)((temporary & 0xFF00) | data);
                        current = temporary;
                        writeNumber = 0;
                    }
                    break;
                case 0x2007:
                    memory.Write(current, data);
                    current += (ushort)(currentIncrement);
                    break;
                case 0x4014:
                    ushort startAddr = (ushort)(data << 8);
                    bus.CpuRam.ReadBufWrapping(oam, oamAddress, startAddr, 256);
                    bus.Cpu.AddIdleCycles(513);
                    if (bus.Cpu.Cycles % 2 == 1)
                    {
                        bus.Cpu.AddIdleCycles(1);
                    }
                    break;
                default:
                    throw new Exception("Hia írás közben");
            }
        }

    }
}

