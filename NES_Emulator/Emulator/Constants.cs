﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================

namespace Apro.Alexandra.Nes
{
    public static class Constants
    {
        public const int RamSize = 64 * 1024;
        public const int Header = 0x1A53454E;
    }
}
