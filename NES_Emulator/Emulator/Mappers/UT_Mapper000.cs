﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================
using Apro.Alexandra.Nes.IO;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Apro.Alexandra.Nes.Mappers
{
    [TestFixture]
    public class UT_Mapper000
    {
        private Bus bus;
        private Mapper000 mapper;

        [SetUp]
        public void SetUp()
        {
            bus = new Bus();
            bus.Cart = new Cartridge(@"C:\Users\36309\Downloads\mario.nes");
            mapper = new Mapper000(bus);
        }

        [TestCase((ushort)0x2000, 0)]
        [TestCase((ushort)0x800, -2048)]
        [TestCase((ushort)0x1000, 0)]
        [TestCase((ushort)0x200, -3584)]
        [TestCase((ushort)0x450, -2992)]
        public void EnsureThat_CurrentRamToIndex_Works_Correctly(ushort address, int expectedIndex)
        {
            int sut = mapper.CurrentRamAddressToIndex(address);
            Assert.AreEqual(expectedIndex, sut);
        }
    }
}
