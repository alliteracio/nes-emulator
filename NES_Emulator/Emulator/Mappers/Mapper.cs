﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================

namespace Apro.Alexandra.Nes.Mappers
{
    public abstract class Mapper
    {
        public enum Mirror
        {
            Horisontal,
            Vertical,
            Low,
            High,
        };

        protected Bus bus;
        protected Mirror mirrorType;

        public virtual void Clock() { }
        public abstract byte Read(ushort address);
        public abstract void Write(ushort address, byte data);
        public int CurrentRamAddressToIndex(ushort address)
        {
            int index = (address - 0x2000) % 0x1000;
            switch (mirrorType)
            {
                case Mirror.Vertical:
                    if (index >= 0x800) index -= 0x800;
                    break;
                case Mirror.Horisontal:
                    if (index > 0x800) index = ((index - 0x800) % 0x400) + 0x400;
                    else index %= 0x400; 
                    break;
                case Mirror.Low:
                    index %= 0x400;
                    break;
                case Mirror.High:
                    index = (index % 400) + 0x400;
                    break;
            }
            return index;
        }
    }
}
