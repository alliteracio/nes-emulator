﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================

namespace Apro.Alexandra.Nes.Mappers
{
    public class Mapper000 : Mapper
	{
		public Mapper000(Bus Bus) 
		{
			bus = Bus;
			mirrorType = Bus.Cart.IsVerticalMirrored ? Mirror.Vertical : Mirror.Horisontal;
		}

		int AddressToPrgRomIndex(ushort address)
		{
			ushort mappedAddress = (ushort)(address - 0x8000); 
			return bus.Cart.PrgBanks == 1 ? (ushort)(mappedAddress % 16384) : mappedAddress; 
		}

        public override byte Read(ushort address)
        {
            byte data;
            if (address < 0x2000)
            {
                data = bus.Cart.ReadChr(address);
            }
            else if (address >= 0x8000)
            {
                data = bus.Cart.ReadPrgRom(AddressToPrgRomIndex(address));
            }
            else
            {
                data = 0;
            }
            return data;
        }

        public override void Write(ushort address, byte data)
        {
            if (address < 0x2000) 
            {
                bus.Cart.WriteChr(address, data);
            }
        }
    }

}
