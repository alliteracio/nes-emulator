﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================
using Apro.Alexandra.Nes.IO;
using Apro.Alexandra.Nes.Mappers;
using Apro.Alexandra.Nes.Memories;
using System;
using System.Diagnostics;
using System.Threading;

namespace Apro.Alexandra.Nes
{
	public class Bus
	{
        public Cpu6502 Cpu { get; set; }
		public Ppu2c02 Ppu { get; set; }
		public Mapper Mapper { get; set; }

		public CpuMemory CpuRam { get; set; }
		public PpuMemory PpuRam { get; set; }

		public Cartridge Cart { get; set; }
		public Controller Controller { get; set; }

		public Action<byte[]> DrawAction { get; set; }

		public bool Stop { get; set; }

		private bool isFrameEven;


		public Bus()
		{
			Controller = new Controller();

			CpuRam = new CpuMemory(this);
			PpuRam = new PpuMemory(this);

			Cpu = new Cpu6502(this);
			Ppu = new Ppu2c02(this);
        }

		public bool TryInsertCartridge(string path) 
		{
			System.Console.WriteLine("Jéték beolvasása ..." + path);
			Cart = new Cartridge(path);
			CreateMapper();

			Cpu.Reset();
			Ppu.Reset();

			CpuRam.Reset();
			PpuRam.Reset();

			isFrameEven = false;
			return true;
		}

		private void CreateMapper()
		{
			System.Console.Write("iNES Mapper azonosító: " + Cart.MapperId.ToString());
			switch (Cart.MapperId)
			{
				case 0:
					System.Console.WriteLine(" támogatott!");
					Mapper = new Mapper000(this);
					Cart.IsInvalid = false;
					break;
				default:
					System.Console.WriteLine(" nem támogatott!");
					Cart.IsInvalid = true;
					break;
			}
		}

		public void DrawFrame()
		{
			DrawAction(Ppu.BitmapData);
			isFrameEven = !isFrameEven;
		}

		void ClockUntilFrame()
		{
			bool orig = isFrameEven;
			while (orig == isFrameEven)
			{
				int cpuCycles = Cpu.Clock();

				for (int i = 0; i < cpuCycles * 3; i++)
				{
					Ppu.Clock();
					Mapper.Clock();
				}
			}
		}

		public void Clock()
		{
			Stop = false;
			byte[] bitmapData = Ppu.BitmapData;

			while (!Stop)
			{
				Stopwatch frameWatch = Stopwatch.StartNew();
				ClockUntilFrame();
				frameWatch.Stop();

				long timeTaken = frameWatch.ElapsedMilliseconds;

				int sleepTime = (int)((1000.0 / 60) - timeTaken);
				Thread.Sleep(Math.Max(sleepTime, 0));
			}
		}

	}
}

