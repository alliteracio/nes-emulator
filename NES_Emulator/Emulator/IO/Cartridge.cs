﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================
using Apro.Alexandra.Nes.Mappers;
using System;
using System.Collections.Generic;
using System.IO;

namespace Apro.Alexandra.Nes.IO
{
    public class Cartridge
    {
        public byte MapperId { get; private set; }
        public byte PrgBanks { get; private set; }
        public byte ChrBanks { get; private set; }

        private byte[] prgRom;
        private byte[] prgRam;
        private byte[] chrRom;

        private int mirrorFlag;
        private int mapperNumber;

        public bool IsInvalid { get; set; }
        public bool IsChrRomUsed { get; private set; }
        public bool IsVerticalMirrored { get; private set; }
        public bool IsBatteryBackedMemory { get; private set; }
        public bool IsContainsTrainer { get; private set; }

        public Cartridge(string path)
        {
            FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            BinaryReader reader = new BinaryReader(stream);
            IsInvalid = false;
            LoadHeader(reader);
            LoadPrgRom(reader);
            LoadChrRom(reader);
            CreatePrgRam();
        }     

        private void LoadHeader(BinaryReader reader)
        {
            if (reader.ReadUInt32() != Constants.Header)
            {
                System.Console.WriteLine("A header nem megfelelő.");
                IsInvalid = true;
                return;
            }

            PrgBanks = reader.ReadByte();
            System.Console.WriteLine((16 * PrgBanks).ToString() + "Kb PRG ROM");

            ChrBanks = reader.ReadByte();
            if (ChrBanks == 0)
            {
                ChrBanks = 2;
                IsChrRomUsed = true;
            }
            else
            {
                System.Console.WriteLine((8 * ChrBanks).ToString() + "Kb CHR ROM");
                IsChrRomUsed = false;
            }

            mirrorFlag = reader.ReadByte();
            IsVerticalMirrored = (mirrorFlag & 0x01) != 0;
            System.Console.WriteLine("A RAM " + (IsVerticalMirrored ? "vertikálisan van tükrözve." : "horizontálisan van tükrözve."));
            IsBatteryBackedMemory = (mirrorFlag & 0x02) != 0;
            IsContainsTrainer = (mirrorFlag & 0x04) != 0;

            mapperNumber = reader.ReadByte();
            MapperId = (byte)(mapperNumber & 0xF0 | (mirrorFlag >> 4 & 0xF));
        }

        private void LoadPrgRom(BinaryReader reader)
        {
            int _prgRomOffset = IsContainsTrainer ? 16 + 512 : 16;
            reader.BaseStream.Seek(_prgRomOffset, SeekOrigin.Begin);
            prgRom = new byte[PrgBanks * 16384];
            reader.Read(prgRom, 0, PrgBanks * 16384);
        }

        private void LoadChrRom(BinaryReader reader)
        {
            if (IsChrRomUsed)
            {
                chrRom = new byte[8192];
            }
            else
            {
                chrRom = new byte[ChrBanks * 8192];
                reader.Read(chrRom, 0, ChrBanks * 8192);
            }
        }

        private void CreatePrgRam()
        { 
            prgRam = new byte[8192];
        }

        public byte ReadPrgRom(int index)
        {
            return prgRom[index];
        }

        public byte ReadPrgRam(int index)
        {
            return prgRam[index];
        }

        public void WritePrgRam(int index, byte data)
        {
            prgRam[index] = data;
        }

        public byte ReadChr(int index)
        {
            return chrRom[index];
        }

        public void WriteChr(int index, byte data)
        {
            if (!IsChrRomUsed) throw new Exception("Írási kísérlet a CHR ROM-ra.");
            chrRom[index] = data;
        }
    }
    
}
