﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================

namespace Apro.Alexandra.Nes.IO
{
    public class Controller
    {
        private bool[] buttonStates;
        private int currentIndex;
        private bool pushed;

        public bool[] ButtonStates { get => buttonStates; private set => buttonStates = value; }

        public Controller()
        {
            buttonStates = new bool[8];
            pushed = false;
        }

        public void SetButtonState(ControllerButton button, bool state)
        {
            buttonStates[(int)button] = state;
        }

        public void SetControllerInput(byte input)
        {
            pushed = (input & 1) == 1;
            if (pushed) currentIndex = 0;
        }

        public byte GetControllerOutput()
        {
            if (currentIndex > 7) return 1;

            bool state = buttonStates[currentIndex];
            if (!pushed) currentIndex++;

            return (byte)(state ? 1 : 0);
        }
    }
}
