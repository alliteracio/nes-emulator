﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Apro.Alexandra.Nes.IO
{
    [TestFixture]
    public class UT_Controller
    {
        private Controller controller;

        [SetUp]
        public void SetUp()
        {
            controller = new Controller();
        }

        [Test]
        public void EnsureThat_SetButtonState_Works_Correctly()
        {
            controller.SetButtonState(ControllerButton.A, true);
            controller.SetButtonState(ControllerButton.B, false);

            Assert.AreEqual(true, controller.ButtonStates[0]);
            Assert.AreEqual(false, controller.ButtonStates[1]);
        }
    }
}
