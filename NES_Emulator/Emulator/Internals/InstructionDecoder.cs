﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================

namespace Apro.Alexandra.Nes.Internals
{
	public class InstructionDecoder
    {
        private readonly Cpu6502 cpu;
        private Instruction[] lookup;

		public InstructionDecoder(Cpu6502 cpu6502)
        {
            cpu = cpu6502;
            lookup = new Instruction[] {
				new Instruction( "BRK",  BRK,  cpu.IMP, 7, 0, 1),
				new Instruction( "ORA",  ORA,  cpu.IZX, 6, 0, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZX, 8, 0, 0),
				new Instruction( "???",  NOP,  cpu.ZP0, 3, 0, 2),
				new Instruction( "ORA",  ORA,  cpu.ZP0, 3, 0, 2),
				new Instruction( "ASL",  ASL,  cpu.ZP0, 5, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZP0, 5, 0, 0),
				new Instruction( "PHP",  PHP,  cpu.IMP, 3, 0, 1),
				new Instruction( "ORA",  ORA,  cpu.IMM, 2, 0, 2),
				new Instruction( "ASL",  ASL,  cpu.ACC, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.IMM, 2, 0, 0),
				new Instruction( "???",  NOP,  cpu.ABS, 4, 0, 3),
				new Instruction( "ORA",  ORA,  cpu.ABS, 4, 0, 3),
				new Instruction( "ASL",  ASL,  cpu.ABS, 6, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABS, 6, 0, 0),

				new Instruction( "BPL",  BPL,  cpu.REL, 2, 1, 2),
				new Instruction( "ORA",  ORA,  cpu.IZY, 5, 1, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZY, 8, 0, 0),
				new Instruction( "???",  NOP,  cpu.ZPX, 4, 0, 2),
				new Instruction( "ORA",  ORA,  cpu.ZPX, 4, 0, 2),
				new Instruction( "ASL",  ASL,  cpu.ZPX, 6, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZPX, 6, 0, 0),
				new Instruction( "CLC",  CLC,  cpu.IMP, 2, 0, 1),
				new Instruction( "ORA",  ORA,  cpu.ABY, 4, 1, 3),
				new Instruction( "???",  NOP,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.ABY, 7, 0, 0),
				new Instruction( "???",  NOP,  cpu.ABX, 4, 1, 3),
				new Instruction( "ORA",  ORA,  cpu.ABX, 4, 1, 3),
				new Instruction( "ASL",  ASL,  cpu.ABX, 7, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABX, 7, 0, 0),

				new Instruction( "JSR",  JSR,  cpu.ABS, 6, 0, 3),
				new Instruction( "AND",  AND,  cpu.IZX, 6, 0, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZX, 8, 0, 0),
				new Instruction( "BIT",  BIT,  cpu.ZP0, 3, 0, 2),
				new Instruction( "AND",  AND,  cpu.ZP0, 3, 0, 2),
				new Instruction( "ROL",  ROL,  cpu.ZP0, 5, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZP0, 5, 0, 0),
				new Instruction( "PLP",  PLP,  cpu.IMP, 4, 0, 1),
				new Instruction( "AND",  AND,  cpu.IMM, 2, 0, 2),
				new Instruction( "ROL",  ROL,  cpu.ACC, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.IMM, 2, 0, 0),
				new Instruction( "BIT",  BIT,  cpu.ABS, 4, 0, 3),
				new Instruction( "AND",  AND,  cpu.ABS, 4, 0, 3),
				new Instruction( "ROL",  ROL,  cpu.ABS, 6, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABS, 6, 0, 0),

				new Instruction( "BMI",  BMI,  cpu.REL, 2, 1, 2),
				new Instruction( "AND",  AND,  cpu.IZY, 5, 1, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZY, 8, 0, 0),
				new Instruction( "???",  NOP,  cpu.ZPX, 4, 0, 2),
				new Instruction( "AND",  AND,  cpu.ZPX, 4, 0, 2),
				new Instruction( "ROL",  ROL,  cpu.ZPX, 6, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZPX, 6, 0, 0),
				new Instruction( "SEC",  SEC,  cpu.IMP, 2, 0, 1),
				new Instruction( "AND",  AND,  cpu.ABY, 4, 1, 3),
				new Instruction( "???",  NOP,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.ABY, 7, 0, 0),
				new Instruction( "???",  NOP,  cpu.ABX, 4, 1, 3),
				new Instruction( "AND",  AND,  cpu.ABX, 4, 1, 3),
				new Instruction( "ROL",  ROL,  cpu.ABX, 7, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABX, 7, 0, 0),

				new Instruction( "RTI",  RTI,  cpu.IMP, 6, 0, 1),
				new Instruction( "EOR",  EOR,  cpu.IZX, 6, 0, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZX, 8, 0, 0),
				new Instruction( "???",  NOP,  cpu.ZP0, 3, 0, 2),
				new Instruction( "EOR",  EOR,  cpu.ZP0, 3, 0, 2),
				new Instruction( "LSR",  LSR,  cpu.ZP0, 5, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZP0, 5, 0, 0),
				new Instruction( "PHA",  PHA,  cpu.IMP, 3, 0, 1),
				new Instruction( "EOR",  EOR,  cpu.IMM, 2, 0, 2),
				new Instruction( "LSR",  LSR,  cpu.ACC, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.IMM, 2, 0, 0),
				new Instruction( "JMP",  JMP,  cpu.ABS, 3, 0, 3),
				new Instruction( "EOR",  EOR,  cpu.ABS, 4, 0, 3),
				new Instruction( "LSR",  LSR,  cpu.ABS, 6, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABS, 6, 0, 0),

				new Instruction( "BVC",  BVC,  cpu.REL, 2, 1, 2),
				new Instruction( "EOR",  EOR,  cpu.IZY, 5, 1, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZY, 8, 0, 0),
				new Instruction( "???",  NOP,  cpu.ZPX, 4, 0, 2),
				new Instruction( "EOR",  EOR,  cpu.ZPX, 4, 0, 2),
				new Instruction( "LSR",  LSR,  cpu.ZPX, 6, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZPX, 6, 0, 0),
				new Instruction( "CLI",  CLI,  cpu.IMP, 2, 0, 1),
				new Instruction( "EOR",  EOR,  cpu.ABY, 4, 1, 3),
				new Instruction( "???",  NOP,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.ABY, 7, 0, 0),
				new Instruction( "???",  NOP,  cpu.ABX, 4, 1, 3),
				new Instruction( "EOR",  EOR,  cpu.ABX, 4, 1, 3),
				new Instruction( "LSR",  LSR,  cpu.ABX, 7, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABX, 7, 0, 0),

				new Instruction( "RTS",  RTS,  cpu.IMP, 6, 0, 1),
				new Instruction( "ADC",  ADC,  cpu.IZX, 6, 0, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZX, 8, 0, 0),
				new Instruction( "???",  NOP,  cpu.ZP0, 3, 0, 2),
				new Instruction( "ADC",  ADC,  cpu.ZP0, 3, 0, 2),
				new Instruction( "ROR",  ROR,  cpu.ZP0, 5, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZP0, 5, 0, 0),
				new Instruction( "PLA",  PLA,  cpu.IMP, 4, 0, 1),
				new Instruction( "ADC",  ADC,  cpu.IMM, 2, 0, 2),
				new Instruction( "ROR",  ROR,  cpu.ACC, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.IMM, 2, 0, 0),
				new Instruction( "JMP",  JMP,  cpu.IND, 5, 0, 3),
				new Instruction( "ADC",  ADC,  cpu.ABS, 4, 0, 3),
				new Instruction( "ROR",  ROR,  cpu.ABS, 6, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABS, 6, 0, 0),

				new Instruction( "BVS",  BVS,  cpu.REL, 2, 1, 2),
				new Instruction( "ADC",  ADC,  cpu.IZY, 5, 1, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZY, 8, 0, 0),
				new Instruction( "???",  NOP,  cpu.ZPX, 4, 0, 2),
				new Instruction( "ADC",  ADC,  cpu.ZPX, 4, 0, 2),
				new Instruction( "ROR",  ROR,  cpu.ZPX, 6, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZPX, 6, 0, 0),
				new Instruction( "SEI",  SEI,  cpu.IMP, 2, 0, 1),
				new Instruction( "ADC",  ADC,  cpu.ABY, 4, 1, 3),
				new Instruction( "???",  NOP,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.ABY, 7, 0, 0),
				new Instruction( "???",  NOP,  cpu.ABX, 4, 1, 3),
				new Instruction( "ADC",  ADC,  cpu.ABX, 4, 1, 3),
				new Instruction( "ROR",  ROR,  cpu.ABX, 7, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABX, 7, 0, 0),

				new Instruction( "???",  NOP,  cpu.IMM, 2, 0, 2),
				new Instruction( "STA",  STA,  cpu.IZX, 6, 0, 2),
				new Instruction( "???",  NOP,  cpu.IMM, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZX, 6, 0, 0),
				new Instruction( "STY",  STY,  cpu.ZP0, 3, 0, 2),
				new Instruction( "STA",  STA,  cpu.ZP0, 3, 0, 2),
				new Instruction( "STX",  STX,  cpu.ZP0, 3, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZP0, 3, 0, 0),
				new Instruction( "DEY",  DEY,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  NOP,  cpu.IMM, 2, 0, 0),
				new Instruction( "TXA",  TXA,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.IMM, 2, 0, 0),
				new Instruction( "STY",  STY,  cpu.ABS, 4, 0, 3),
				new Instruction( "STA",  STA,  cpu.ABS, 4, 0, 3),
				new Instruction( "STX",  STX,  cpu.ABS, 4, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABS, 4, 0, 0),

				new Instruction( "BCC",  BCC,  cpu.REL, 2, 1, 2),
				new Instruction( "STA",  STA,  cpu.IZY, 6, 0, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZY, 6, 0, 0),
				new Instruction( "STY",  STY,  cpu.ZPX, 4, 0, 2),
				new Instruction( "STA",  STA,  cpu.ZPX, 4, 0, 2),
				new Instruction( "STX",  STX,  cpu.ZPY, 4, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZPY, 4, 0, 0),
				new Instruction( "TYA",  TYA,  cpu.IMP, 2, 0, 1),
				new Instruction( "STA",  STA,  cpu.ABY, 5, 0, 3),
				new Instruction( "TXS",  TXS,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.ABY, 5, 0, 0),
				new Instruction( "???",  NOP,  cpu.ABX, 5, 0, 0),
				new Instruction( "STA",  STA,  cpu.ABX, 5, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABY, 5, 0, 0),
				new Instruction( "???",  XXX,  cpu.ABY, 5, 0, 0),

				new Instruction( "LDY",  LDY,  cpu.IMM, 2, 0, 2),
				new Instruction( "LDA",  LDA,  cpu.IZX, 6, 0, 2),
				new Instruction( "LDX",  LDX,  cpu.IMM, 2, 0, 2),
				new Instruction( "???",  XXX,  cpu.IZX, 6, 0, 0),
				new Instruction( "LDY",  LDY,  cpu.ZP0, 3, 0, 2),
				new Instruction( "LDA",  LDA,  cpu.ZP0, 3, 0, 2),
				new Instruction( "LDX",  LDX,  cpu.ZP0, 3, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZP0, 3, 0, 0),
				new Instruction( "TAY",  TAY,  cpu.IMP, 2, 0, 1),
				new Instruction( "LDA",  LDA,  cpu.IMM, 2, 0, 2),
				new Instruction( "TAX",  TAX,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.IMM, 2, 0, 0),
				new Instruction( "LDY",  LDY,  cpu.ABS, 4, 0, 3),
				new Instruction( "LDA",  LDA,  cpu.ABS, 4, 0, 3),
				new Instruction( "LDX",  LDX,  cpu.ABS, 4, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABS, 4, 0, 0),

				new Instruction( "BCS",  BCS,  cpu.REL, 2, 1, 2),
				new Instruction( "LDA",  LDA,  cpu.IZY, 5, 1, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZY, 5, 1, 0),
				new Instruction( "LDY",  LDY,  cpu.ZPX, 4, 0, 2),
				new Instruction( "LDA",  LDA,  cpu.ZPX, 4, 0, 2),
				new Instruction( "LDX",  LDX,  cpu.ZPY, 4, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZPY, 4, 0, 0),
				new Instruction( "CLV",  CLV,  cpu.IMP, 2, 0, 1),
				new Instruction( "LDA",  LDA,  cpu.ABY, 4, 1, 3),
				new Instruction( "TSX",  TSX,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.ABY, 4, 1, 0),
				new Instruction( "LDY",  LDY,  cpu.ABX, 4, 1, 3),
				new Instruction( "LDA",  LDA,  cpu.ABX, 4, 1, 3),
				new Instruction( "LDX",  LDX,  cpu.ABY, 4, 1, 3),
				new Instruction( "???",  XXX,  cpu.ABY, 4, 1, 0),

				new Instruction( "CPY",  CPY,  cpu.IMM, 2, 0, 2),
				new Instruction( "CMP",  CMP,  cpu.IZX, 6, 0, 2),
				new Instruction( "???",  NOP,  cpu.IMM, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZX, 8, 0, 0),
				new Instruction( "CPY",  CPY,  cpu.ZP0, 3, 0, 2),
				new Instruction( "CMP",  CMP,  cpu.ZP0, 3, 0, 2),
				new Instruction( "DEC",  DEC,  cpu.ZP0, 5, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZP0, 5, 0, 0),
				new Instruction( "INY",  INY,  cpu.IMP, 2, 0, 1),
				new Instruction( "CMP",  CMP,  cpu.IMM, 2, 0, 2),
				new Instruction( "DEX",  DEX,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.IMM, 2, 0, 0),
				new Instruction( "CPY",  CPY,  cpu.ABS, 4, 0, 3),
				new Instruction( "CMP",  CMP,  cpu.ABS, 4, 0, 3),
				new Instruction( "DEC",  DEC,  cpu.ABS, 6, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABS, 6, 0, 0),

				new Instruction( "BNE",  BNE,  cpu.REL, 2, 1, 2),
				new Instruction( "CMP",  CMP,  cpu.IZY, 5, 1, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZY, 8, 0, 0),
				new Instruction( "???",  NOP,  cpu.ZPX, 4, 0, 2),
				new Instruction( "CMP",  CMP,  cpu.ZPX, 4, 0, 2),
				new Instruction( "DEC",  DEC,  cpu.ZPX, 6, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZPX, 6, 0, 0),
				new Instruction( "CLD",  CLD,  cpu.IMP, 2, 0, 1),
				new Instruction( "CMP",  CMP,  cpu.ABY, 4, 1, 3),
				new Instruction( "NOP",  NOP,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.ABY, 7, 0, 0),
				new Instruction( "???",  NOP,  cpu.ABX, 4, 1, 3),
				new Instruction( "CMP",  CMP,  cpu.ABX, 4, 1, 3),
				new Instruction( "DEC",  DEC,  cpu.ABX, 7, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABX, 7, 0, 0),

				new Instruction( "CPX",  CPX,  cpu.IMM, 2, 0, 2),
				new Instruction( "SBC",  SBC,  cpu.IZX, 6, 0, 2),
				new Instruction( "???",  NOP,  cpu.IMM, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZX, 8, 0, 0),
				new Instruction( "CPX",  CPX,  cpu.ZP0, 3, 0, 2),
				new Instruction( "SBC",  SBC,  cpu.ZP0, 3, 0, 2),
				new Instruction( "INC",  INC,  cpu.ZP0, 5, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZP0, 5, 0, 0),
				new Instruction( "INX",  INX,  cpu.IMP, 2, 0, 1),
				new Instruction( "SBC",  SBC,  cpu.IMM, 2, 0, 2),
				new Instruction( "NOP",  NOP,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  SBC,  cpu.IMM, 2, 0, 0),
				new Instruction( "CPX",  CPX,  cpu.ABS, 4, 0, 3),
				new Instruction( "SBC",  SBC,  cpu.ABS, 4, 0, 3),
				new Instruction( "INC",  INC,  cpu.ABS, 6, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABS, 6, 0, 0),

				new Instruction( "BEQ",  BEQ,  cpu.REL, 2, 1, 2),
				new Instruction( "SBC",  SBC,  cpu.IZY, 5, 1, 2),
				new Instruction( "???",  XXX,  cpu.IMP, 2, 0, 0),
				new Instruction( "???",  XXX,  cpu.IZY, 8, 0, 0),
				new Instruction( "???",  NOP,  cpu.ZPX, 4, 0, 2),
				new Instruction( "SBC",  SBC,  cpu.ZPX, 4, 0, 2),
				new Instruction( "INC",  INC,  cpu.ZPX, 6, 0, 2),
				new Instruction( "???",  XXX,  cpu.ZPX, 6, 0, 0),
				new Instruction( "SED",  SED,  cpu.IMP, 2, 0, 1),
				new Instruction( "SBC",  SBC,  cpu.ABY, 4, 1, 3),
				new Instruction( "NOP",  NOP,  cpu.IMP, 2, 0, 1),
				new Instruction( "???",  XXX,  cpu.ABY, 7, 0, 0),
				new Instruction( "???",  NOP,  cpu.ABX, 4, 1, 3),
				new Instruction( "SBC",  SBC,  cpu.ABX, 4, 1, 3),
				new Instruction( "INC",  INC,  cpu.ABX, 7, 0, 3),
				new Instruction( "???",  XXX,  cpu.ABX, 7, 0, 0),

			};
		}

		public Instruction this[byte opcode]
        {
			get
            {
				return lookup[opcode];
            }
        }

		private void XXX(Cpu6502.AddressMode mode, ushort address)
		{

		}

		private void NOP(Cpu6502.AddressMode mode, ushort address)
		{

		}

		private void ADC(Cpu6502.AddressMode mode, ushort address)
		{

			byte data = cpu.memory.Read(address);
			int carry = (cpu.C ? 1 : 0);

			byte sum = (byte)(cpu.A + data + carry);
			cpu.SetSpecialFlags(sum);

			cpu.C = (cpu.A + data + carry) > 0xFF;

			cpu.V = (~(cpu.A ^ data) & (cpu.A ^ sum) & 0x80) != 0;

			cpu.A = sum;
		}

		private void SBC(Cpu6502.AddressMode mode, ushort address)
		{
			byte data = cpu.memory.Read(address);
			int notCarry = (!cpu.C ? 1 : 0);

			byte result = (byte)(cpu.A - data - notCarry);
			cpu.SetSpecialFlags(result);

			cpu.C = (cpu.A - data - notCarry) >= 0 ? true : false;

			cpu.V = ((cpu.A ^ data) & (cpu.A ^ result) & 0x80) != 0;

			cpu.A = result;
		}

		private void BRK(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.PushStack16(cpu.PC);
			cpu.PushStack(cpu.GetStatusFlags());
			cpu.B = true;
			cpu.PC = cpu.memory.Read16((ushort)0xFFFE);
		}

		private void AND(Cpu6502.AddressMode mode, ushort address)
		{
			byte data = cpu.memory.Read(address);
			cpu.A &= data;
			cpu.SetSpecialFlags(cpu.A);
		}
		private void ASL(Cpu6502.AddressMode mode, ushort address)
		{
			if (mode == Cpu6502.AddressMode.Accumulator)
			{
				cpu.C = cpu.IsBitSet(cpu.A, 7);
				cpu.A <<= 1;
				cpu.SetSpecialFlags(cpu.A);
			}
			else
			{
				byte data = cpu.memory.Read(address);
				cpu.C = cpu.IsBitSet(data, 7);
				byte dataUpdated = (byte)(data << 1);
				cpu.memory.Write(address, dataUpdated);
				cpu.SetSpecialFlags(dataUpdated);
			}
		}

		private void BCC(Cpu6502.AddressMode mode, ushort address)
		{
			if (!cpu.C)
			{
				cpu.HandleBranchCycles(cpu.PC, address);
				cpu.PC = address;
			}
		}

		private void BCS(Cpu6502.AddressMode mode, ushort address)
		{
			if (cpu.C)
			{
				cpu.HandleBranchCycles(cpu.PC, address);
				cpu.PC = address;
			}
		}

		private void BEQ(Cpu6502.AddressMode mode, ushort address)
		{
			if (cpu.Z)
			{
				cpu.HandleBranchCycles(cpu.PC, address);
				cpu.PC = address;
			}
		}

		private void BIT(Cpu6502.AddressMode mode, ushort address)
		{
			byte data = cpu.memory.Read(address);
			cpu.N = cpu.IsBitSet(data, 7);
			cpu.V = cpu.IsBitSet(data, 6);
			cpu.Z = (data & cpu.A) == 0;
		}

		private void BMI(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.PC = cpu.N ? address : cpu.PC;
		}

		private void BNE(Cpu6502.AddressMode mode, ushort address)
		{
			if (!cpu.Z)
			{
				cpu.HandleBranchCycles(cpu.PC, address);
				cpu.PC = address;
			}
		}

		private void BPL(Cpu6502.AddressMode mode, ushort address)
		{
			if (!cpu.N)
			{
				cpu.HandleBranchCycles(cpu.PC, address);
				cpu.PC = address;
			}

		}

		private void BVC(Cpu6502.AddressMode mode, ushort address)
		{
			if (!cpu.V)
			{
				cpu.HandleBranchCycles(cpu.PC, address);
				cpu.PC = address;
			}
		}

		private void BVS(Cpu6502.AddressMode mode, ushort address)
		{
			if (cpu.V)
			{
				cpu.HandleBranchCycles(cpu.PC, address);
				cpu.PC = address;
			}
		}

		private void CLC(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.C = false;
		}


		private void CLD(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.D = false;
		}


		private void CLI(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.I = false;
		}


		private void CLV(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.V = false;
		}

		private void CMP(Cpu6502.AddressMode mode, ushort address)
		{
			byte data = cpu.memory.Read(address);
			cpu.C = cpu.A >= data;
			cpu.SetSpecialFlags((byte)(cpu.A - data));
		}

		private void CPX(Cpu6502.AddressMode mode, ushort address)
		{
			byte data = cpu.memory.Read(address);
			cpu.SetSpecialFlags((byte)(cpu.X - data));
			cpu.C = cpu.X >= data;
		}


		private void CPY(Cpu6502.AddressMode mode, ushort address)
		{
			byte data = cpu.memory.Read(address);
			cpu.SetSpecialFlags((byte)(cpu.Y - data));
			cpu.C = cpu.Y >= data;
		}


		private void DEC(Cpu6502.AddressMode mode, ushort address)
		{
			byte data = cpu.memory.Read(address);
			data--;
			cpu.memory.Write(address, data);
			cpu.SetSpecialFlags(data);
		}


		private void DEX(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.X--;
			cpu.SetSpecialFlags(cpu.X);

		}


		private void DEY(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.Y--;
			cpu.SetSpecialFlags(cpu.Y);
		}


		private void EOR(Cpu6502.AddressMode mode, ushort address)
		{
			byte data = cpu.memory.Read(address);
			cpu.A ^= data;
			cpu.SetSpecialFlags(cpu.A);
		}


		private void INC(Cpu6502.AddressMode mode, ushort address)
		{
			byte data = cpu.memory.Read(address);
			data++;
			cpu.memory.Write(address, data);
			cpu.SetSpecialFlags(data);
		}


		private void INX(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.X++;
			cpu.SetSpecialFlags(cpu.X);
		}


		private void INY(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.Y++;
			cpu.SetSpecialFlags(cpu.Y);
		}


		private void JMP(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.PC = address;
		}


		private void JSR(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.PushStack16((ushort)(cpu.PC - 1));
			cpu.PC = address;
		}


		private void LDA(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.A = cpu.memory.Read(address);
			cpu.SetSpecialFlags(cpu.A);
		}


		private void LDX(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.X = cpu.memory.Read(address);
			cpu.SetSpecialFlags(cpu.X);
		}


		private void LDY(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.Y = cpu.memory.Read(address);
			cpu.SetSpecialFlags(cpu.Y);
		}

		private void LSR(Cpu6502.AddressMode mode, ushort address)
		{
			if (mode == Cpu6502.AddressMode.Accumulator)
			{
				cpu.C = (cpu.A & 1) == 1;
				cpu.A >>= 1;

				cpu.SetSpecialFlags(cpu.A);
			}
			else
			{
				byte value = cpu.memory.Read(address);
				cpu.C = (value & 1) == 1;

				byte updatedValue = (byte)(value >> 1);

				cpu.memory.Write(address, updatedValue);

				cpu.SetSpecialFlags(updatedValue);
			}
		}

		private void ORA(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.A |= cpu.memory.Read(address);
			cpu.SetSpecialFlags(cpu.A);
		}


		private void PHA(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.PushStack(cpu.A);
		}


		private void PHP(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.PushStack((byte)(cpu.GetStatusFlags() | 0x10));

		}


		private void PLA(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.A = cpu.PullStack();
			cpu.SetSpecialFlags(cpu.A);
		}


		private void PLP(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.SetProcessorFlags((byte)(cpu.PullStack() & ~(0x10)));
		}

		private void ROL(Cpu6502.AddressMode mode, ushort address)
		{
			bool Corig = cpu.C;
			if (mode == Cpu6502.AddressMode.Accumulator)
			{
				cpu.C = cpu.IsBitSet(cpu.A, 7);
				cpu.A <<= 1;
				cpu.A |= (byte)(Corig ? 1 : 0);

				cpu.SetSpecialFlags(cpu.A);
			}
			else
			{
				byte data = cpu.memory.Read(address);
				cpu.C = cpu.IsBitSet(data, 7);

				data <<= 1;
				data |= (byte)(Corig ? 1 : 0);

				cpu.memory.Write(address, data);

				cpu.SetSpecialFlags(data);
			}
		}

		private void ROR(Cpu6502.AddressMode mode, ushort address)
		{
			bool Corig = cpu.C;
			if (mode == Cpu6502.AddressMode.Accumulator)
			{
				cpu.C = cpu.IsBitSet(cpu.A, 0);
				cpu.A >>= 1;
				cpu.A |= (byte)(Corig ? 0x80 : 0);

				cpu.SetSpecialFlags(cpu.A);
			}
			else
			{
				byte data = cpu.memory.Read(address);
				cpu.C = cpu.IsBitSet(data, 0);

				data >>= 1;
				data |= (byte)(Corig ? 0x80 : 0);

				cpu.memory.Write(address, data);

				cpu.SetSpecialFlags(data);
			}
		}

		private void RTI(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.SetProcessorFlags(cpu.PullStack());
			cpu.PC = cpu.PullStack16();
		}

		private void RTS(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.PC = (ushort)(cpu.PullStack16() + 1);
		}


		private void SEC(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.C = true;
		}


		private void SED(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.D = true;
		}


		private void SEI(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.I = true;

		}


		private void STA(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.memory.Write(address, cpu.A);
		}


		private void STX(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.memory.Write(address, cpu.X);
		}


		private void STY(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.memory.Write(address, cpu.Y);
		}


		private void TAX(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.X = cpu.A;
			cpu.SetSpecialFlags(cpu.X);
		}


		private void TAY(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.Y = cpu.A;
			cpu.SetSpecialFlags(cpu.Y);
		}


		private void TSX(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.X = cpu.SP;
			cpu.SetSpecialFlags(cpu.X);
		}


		private void TXA(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.A = cpu.X;
			cpu.SetSpecialFlags(cpu.A);
		}


		private void TXS(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.SP = cpu.X;
		}


		private void TYA(Cpu6502.AddressMode mode, ushort address)
		{
			cpu.A = cpu.Y;
			cpu.SetSpecialFlags(cpu.A);
		}
	}
}
