﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================
using System;

namespace Apro.Alexandra.Nes.Internals
{
	public class Instruction
	{
		public string Name { get; }
		public Action<Cpu6502.AddressMode,ushort> Operate { get; }
		public Func<Cpu6502.AddressMode> Addrmode { get; }
		public byte Cycles { get; }
		public int PageCycles { get; }
		public int Size { get; }

		public Instruction(string name, Action<Cpu6502.AddressMode, ushort> operate, Func<Cpu6502.AddressMode> addrmode, byte cycles, int page, int size)
		{
			Name = name;
			Operate = operate;
			Addrmode = addrmode;
			Cycles = cycles;
			PageCycles = page;
			Size = size;
		}
	};
}