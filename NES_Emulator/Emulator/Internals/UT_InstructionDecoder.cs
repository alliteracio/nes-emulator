﻿//=================================================================================
// Apro Aleksandra - NES Emulator 2020/2021 - Szakdolgozat
//=================================================================================
using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace Apro.Alexandra.Nes.Internals
{
    [TestFixture]
    public class UT_InstructionDecoder
    {
        private InstructionDecoder decoder;
        private Cpu6502 cpu;
        private Bus bus;

        [SetUp]
        public void SetUp()
        {
            bus = new Bus();
            cpu = new Cpu6502(bus);
            decoder = new InstructionDecoder(cpu);
        }

        [TestCase(5,"ORA",3)]
        [TestCase(15, "???", 6)]
        [TestCase(16, "BPL", 2)]
        [TestCase(32, "JSR", 6)]
        [TestCase(48, "BMI", 2)]
        public void EnsureThat_InsructionDecoder_Works_Correctly(byte opcode, string expectedName, byte expectedCycles)
        {
            Instruction sut = decoder[opcode];
            Assert.AreEqual(expectedName, sut.Name);
            Assert.AreEqual(expectedCycles, sut.Cycles);
        }
    }
}
